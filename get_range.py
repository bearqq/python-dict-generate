#coding: utf-8
#14-10-17
#some definitions:
cityurl='http://www.hiphop8.com/city/chongqing/zhongqing.php'
useproxy=1
proxyaddr='http://127.0.0.1:8087'

#imports
import re,urllib,urllib2,codecs,random

#def
def codeiss(data):#decode
	reg=re.compile('=\"utf-8|=utf-8|=\'utf-8',re.IGNORECASE).search(data)
	if reg and (reg.start()<400):
		return data.decode("UTF-8")
	else:
		return data.decode("GBK")

def randomip():#ip random
	return str(random.randint(0,254))+'.'+str(random.randint(0,254))+'.'+str(random.randint(0,254))+'.'+str(random.randint(0,254))

def fetchurl(url):#fetch from url and then decode
	if useproxy==1:
		proxy_handler = urllib2.ProxyHandler({'http':proxyaddr})
		opener1=urllib2.build_opener(proxy_handler)
	else:
		opener1=urllib2.build_opener()

	request1 = urllib2.Request(url)
	request1.add_header("User-Agent","Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:27.0) Gecko/20100101 Firefox/27.0")
	ip=randomip()
	request1.add_header("X-FORWARDED-FOR",ip)
	request1.add_header("VIA",ip)
	request1.add_header("CLIENT-IP",ip)

	try:
		fdbkf1=opener1.open(request1)
	except:# IOError,e:
		print 'Net work Error!\n'#+' '+str(e.reason)
		return
	else:
		return codeiss(fdbkf1.read())

def main():
	fddata=fetchurl(cityurl)
	if fddata:
		f=codecs.open('phrange.txt','a','utf-8')
		phlist=re.findall('l\">(\d*)</a>',fddata)
		for ph in phlist:
			f.write('%s\n' % ph)
		f.close()
		print "finished!"

	
if __name__ == '__main__':
	main()