#coding: utf-8
#14-10-29

#some definitions:
startyear=1935
endyear=2014

#defines

#imports
import codecs

#def
def writeit(): #写入文件
    fout=codecs.open('dictout.txt','a','utf-8')
    for year in range(startyear,endyear+1):
        for mon in range(1,13):
            for day in range(1,32):
                if (mon in (4,6,9,11)) and (day==31):
                    continue
                if mon==2:
                    if (not (year % 4)) and (day>29):
                        continue
                    if (year % 4) and (day>28):
                        continue
                if mon<10:
                    smon=''.join(('0',str(mon)))
                else:
                    smon=str(mon)
                if day<10:
                    sday=''.join(('0',str(day)))
                else:
                    sday=str(day)
                fout.write('%d%s%s\n' % (year,smon,sday))
    fout.close()

#main
writeit() #文件输入部分
print 'Finished!'

